from PyQt5.QtWidgets import *
from PyQt5.QtCore import *
import sys
import pandas as pd
import json
from os.path import exists


class Window(QMainWindow):
    def __init__(self, parent = None):
        super().__init__(parent)
        self.filename = ("empty 1", "empty 2")
        self.titles = []
        self.dataset = pd.DataFrame()
        self.columnList = QListWidget(self)
        self.initUI()

    def initUI(self):
        self.setGeometry(100, 100, 640, 480)
        self.setWindowTitle("Dataset Toolbox v0.2")

        offset = 10

        self.importbutton = QPushButton(self)
        self.importbutton.setText("Import CSV")
        self.importbutton.move(offset, offset)
        self.importbutton.clicked.connect(self.import_button_click)

        self.exportbutton = QPushButton(self)
        self.exportbutton.setText("Export CSV")
        self.exportbutton.move(offset * 2 + self.importbutton.width(), offset)
        self.exportbutton.clicked.connect(self.export_button_click)

        self.tablelable = QLabel(self)
        self.tablelable.setText("Check the columns that should be kept")
        self.tablelable.setGeometry(offset, 50, self.width() - offset, 20)

        self.columnList.setGeometry(
            offset,
            70,
            self.width() - 20,
            self.height() - (self.tablelable.height() + 50) - offset)
        self.columnList.show()

        self.show()

    def add_table_from_array(self, array):
        if self.columnList.count() > 0:
            self.columnList.clear()
        for title in array:
            item = QListWidgetItem(title)
            item.setCheckState(Qt.Checked)
            self.columnList.addItem(item)

    def open_file_dialog(self):
        self.filename = QFileDialog.getOpenFileName(None, "Import CSV", filter=".csv(*.csv)")
        if exists(self.filename[0]):
            # Process the csv here
            self.dataset = self.process_file(self.filename[0])

            # Add titles to QTableView
            self.titles = self.dataset.columns
            self.add_table_from_array(self.titles)
        else:
            message = QMessageBox()
            message.setWindowTitle("Error:")
            message.setText("Could not open the selected file")
            message.exec_()

    def import_button_click(self):
        self.open_file_dialog()

    def assess_filetype(self, filestring):
        filetype = 0
        file = open(filestring, "r")
        lineone = file.readline()
        if lineone.__contains__("sep"):
            filetype = 1
        if lineone.__contains__("subjectid"):
            filetype = 2
        if lineone.__contains__("sender"):
            filetype = 3
        file.close()
        return filetype

    def process_json(self, raw_data):
        raw_data.columns.values[0] = "subjectid"

        raw_data = raw_data["subjectid"].str.split(';', 1, expand=True)

        raw_data.rename(columns={
            0: "subjectid",
            1: "data"
        }, inplace=True)

        subjectid = raw_data['subjectid']
        subjectid = subjectid.str.replace("'", "")

        data = raw_data['data']
        data = data.str.replace("'", "")

        raw_data['subjectid'] = subjectid
        raw_data['data'] = data

        try:
            # Clean all the JSON cells
            def clean_json(cell):
                return json.loads(cell)

            test = pd.DataFrame()
            for row in raw_data.data:
                test = raw_data['data'].apply(clean_json)

            # Normalize the JSON fields into DataFrame columns
            dataset = pd.DataFrame()
            for i in range(test.count()):
                frame = pd.json_normalize(test.iloc[i])
                frame['subjectid'] = raw_data['subjectid'].iloc[i]
                dataset = dataset.append(frame)

            dataset['sender'] = dataset['sender'].replace(regex=[' '], value='_')
            dataset['sender'] = dataset['sender'] + "-" + dataset['sender_type'] + "-" + dataset['sender_id']
            dataset = dataset.drop(columns=['sender_type', 'sender_id'], axis=1)

            dataset = pd.DataFrame(dataset.set_index(['subjectid', 'sender']).stack()).reset_index()
            dataset['var'] = dataset['sender'] + "-" + dataset['level_2']
            dataset = dataset.drop(['sender', 'level_2'], axis=1).rename(columns={0: 'data'}).groupby(
                ['subjectid', 'var']).data.first().unstack()
            dataset = dataset[sorted(dataset.columns)]

            return dataset
        except:
            message = QMessageBox()
            message.setWindowTitle("Error:")
            message.setText("The selected file could not be processed due to an invalid JSON structure")
            message.exec_()
            return None

    def process_csv(self, raw_data):
        try:
            dataset = raw_data
            dataset['sender'] = dataset['sender'].replace(regex=[' '], value='_')
            dataset['sender'] = dataset['sender'] + "-" + dataset['sender_type'] + "-" + dataset['sender_id']
            dataset = dataset.drop(columns=['sender_type', 'sender_id'], axis=1)
            dataset = pd.DataFrame(dataset.set_index(['sender']).stack()).reset_index()
            dataset['var'] = dataset['sender'] + "-" + dataset['level_1']
            dataset['subjectid'] = 0
            dataset = dataset.drop(['sender', 'level_1'], axis=1).rename(columns={0: 'data'}).groupby(
                ['subjectid', 'var']).data.first().unstack()
            return dataset
        except:
            message = QMessageBox()
            message.setWindowTitle("Error:")
            message.setText(
                "The selected file could not be processed due to an unexpected pattern in the data structure")
            message.exec_()
            return None

    def process_file(self, filestring):
        # Preprocess the file into a manageble dataset
        try:
            # Check if the formatting is a LAB.js formatted data file
            # Does it list a 'sep' on the first line?
            filetype = self.assess_filetype(filestring)

            if filetype == 1:
                raw_data = pd.read_csv(filestring, sep='\n').iloc[1:, :]
                dataset = self.process_json(raw_data)
                return dataset

            if filetype == 2:
                raw_data = pd.read_csv(filestring, sep='\n')
                dataset = self.process_json(raw_data)
                return dataset

            if filetype == 3:
                raw_data = pd.read_csv(filestring, sep=',')
                dataset = self.process_csv(raw_data)
                return dataset
        except:
            message = QMessageBox()
            message.setWindowTitle("Error:")
            message.setText("The selected file could not be processed due to an unexpected dataset structure")
            message.exec_()
            return None

    def export_button_click(self):
        # Get the columns to be dropped from the dataset
        columns_to_drop = []
        for i in range(self.columnList.count()):
            if self.columnList.item(i).checkState() == Qt.Unchecked:
                columns_to_drop.append(self.columnList.item(i).text())

        # Drop unwanted columns
        new_dataset = self.dataset.drop(columns_to_drop, axis=1)

        # Open a filedialog to save the file (pre-fill the original name followed by "_pruned")
        new_filename = self.filename[0]
        new_filename = new_filename.replace(".csv", "")
        new_filename = new_filename + "_pruned.csv"
        new_filename = QFileDialog.getSaveFileName(None, "Save CSV", new_filename, filter=".csv(*.csv)")

        # On closing of the file dialog, actually save the file using the new filename
        new_dataset.to_csv(new_filename[0], sep=";")


if __name__ == '__main__':
    app = QApplication(sys.argv)
    win = Window()
    sys.exit(app.exec_())
